# vueproject

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

create project vue
vue create name

remove lint
npm remove @vue/cli-plugin-eslint

run project
npm run serve

thêm router
vue add router